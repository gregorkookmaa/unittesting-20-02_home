function random(max = 10) {
  return Math.floor(Math.random() * (max + 1));
}

module.exports = random;
